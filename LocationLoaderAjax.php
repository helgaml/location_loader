<?php
include('../../../wp-config.php');
$CurrentURL="http://local.pure-bred.com/";
$servername=DB_HOST;
$DBName=DB_NAME;
$db = new PDO("mysql:host=$servername;dbname=$DBName", DB_USER, DB_PASSWORD);
$GoogleApiCounter=0;
$MaxGoogleApiPerSecond=5;
function FixFrench($Line){
	$Line=str_replace("À","&Agrave;",$Line);	
	$Line=str_replace("à","&agrave;",$Line);	
	$Line=str_replace("Â","&Acirc;",$Line);	
	$Line=str_replace("â","&acirc;",$Line);
	$Line=str_replace("Æ","&AElig;",$Line);	
	$Line=str_replace("æ","&aelig;",$Line);	
	$Line=str_replace("Ç","&Ccedil;",$Line);	
	$Line=str_replace("ç","&ccedil;",$Line);	
	$Line=str_replace("È","&Egrave;",$Line);	
	$Line=str_replace("è","&egrave;",$Line);	
	$Line=str_replace("É","&Eacute;",$Line);	
	$Line=str_replace("é","&eacute;",$Line);	
	$Line=str_replace("Ê","&Ecirc;",$Line);
	$Line=str_replace("ê","&ecirc;",$Line);
	$Line=str_replace("Ë","&Euml;",$Line);	
	$Line=str_replace("ë","&euml;",$Line);	
	$Line=str_replace("Î","&Icirc;",$Line);	
	$Line=str_replace("î","&icirc;",$Line);	
	$Line=str_replace("Ï","&Iuml;",$Line);
	$Line=str_replace("ï","&iuml;",$Line);
	$Line=str_replace("Ô","&Ocirc;",$Line);	
	$Line=str_replace("ô","&ocirc;",$Line);
	$Line=str_replace("Œ","&OElig;",$Line);
	$Line=str_replace("œ","&oelig;",$Line);
	$Line=str_replace("Ù","&Ugrave;",$Line);
	$Line=str_replace("ù","&ugrave;",$Line);
	$Line=str_replace("Û","&Ucirc;",$Line);
	$Line=str_replace("û","&ucirc;",$Line);
	$Line=str_replace("Ü","&Uuml;",$Line);
	$Line=str_replace("ü","&uuml;",$Line);
	$Line=str_replace("«","&laquo;",$Line);
	$Line=str_replace("»","&raquo;",$Line);
	$Line=str_replace("€","&euro;",$Line);
	return $Line;
}
function GetLatLong($Address){
	//global $GoogleLog;
	global $GoogleApiCounter;
	global $MaxGoogleApiPerSecond;
	set_time_limit(0);
	if ($GoogleApiCounter>=$MaxGoogleApiPerSecond){
		$GoogleApiCounter=0;
		sleep(1);
	}
	$URL="http://maps.google.com/maps/api/geocode/json?address=".urlencode($Address);
	
	$ch = curl_init();
	$defaults = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_URL => $URL , 
		CURLOPT_HEADER => false, 
		CURLOPT_POST => false
	);
	$ch = curl_init();
	curl_setopt_array($ch, ($defaults));
	$response = curl_exec($ch);
	curl_close($ch);
	
	$response_a = json_decode($response);
	
	$Status=$response_a->status;
	
	$lat = $response_a->results[0]->geometry->location->lat;
	$long = $response_a->results[0]->geometry->location->lng;
	$Result['Lat']=$lat;
	$Result['Long']=$long;
	$Result['Status']=$Status;
	print_r($Result);
	//sleep(2);
	//fwrite($GoogleLog,"-----------------------------------------------------------");
	//fwrite($GoogleLog,$Address);
	//fwrite($GoogleLog,$response);
	$GoogleApiCounter++;
	return $Result;
}
function GetLatLong_($Address){
	$lat=rand(29,54);
	$long=-1*rand(80,125);
	$Result['Lat']=$lat;
	$Result['Long']=$long;	
	return $Result;
}
function UpdateLocationLatAndLong($Id,$Address){
	global $db;
	$LatLong=GetLatLong($Address);
	$Lat=$LatLong['Lat'];
	$Long=$LatLong['Long'];
	$Status=$LatLong['Status'];
	
	$Sql="UPDATE imax_store_location SET Lat = '$Lat' ,`Long` = '$Long', `GoogleStatus`='$Status' WHERE id_imax_store_location =$Id;";	
	$db->query($Sql);
}
function AddLocation($Brand,$StoreName,$Lat,$Long,$Contact,$Address){
	global $db;
	if ($Lat==''){
		$LatLong=GetLatLong($Address);
		$Lat=$LatLong['Lat'];
		$Long=$LatLong['Long'];
		$Status=$LatLong['Status'];
	} else {
		$Status="NA";
	}
	$Sql="INSERT INTO `imax_store_location` (`Brand`,`StoreName`, `Lat`, `Long`, `Address`,`Contact`,`GoogleStatus`) VALUES ('$Brand','$StoreName', '$Lat', '$Long', '$Address','$Contact','$Status')";
	echo "<BR>$Sql<BR>";
	$db->query($Sql);
}

function StoreExists($Store,$Address){
	global $db;
	$Result=false;
	$Sql="SELECT count(*) as cnt FROM imax_store_location
	WHERE imax_store_location.StoreName = '$Store'
	AND imax_store_location.Address = '$Address'";
	
	echo "<BR>".$Sql."<BR>";
	
	$st=$db->query($Sql);
	foreach ($st as $Rec){
		$Count=$Rec['cnt'];
	}
	if ($Count>0) $Result=true;
	//return $Result;
	return false;
}


function GetStoresList(){
	global $db;
	$Stores=array();
	$Sql="SELECT
		  wp_terms.name,
		  wp_term_taxonomy.term_taxonomy_id
		FROM wp_term_taxonomy
		  INNER JOIN wp_terms
			ON wp_term_taxonomy.term_id = wp_terms.term_id
		WHERE wp_term_taxonomy.taxonomy = 'acf_maps_category'";
	$st=$db->query($Sql);
	foreach ($st as $Rec){
		$Name=$Rec['name'];
		$Id=$Rec['term_taxonomy_id'];
		$Stores[$Name]=$Id;
	}
	return $Stores;
}
function CreateNewStore($Brand){
	global $db;
	$slug=strtolower(str_replace("'","_",$Brand));
	$Brand=str_replace("'","\'",$Brand);
	$Sql="INSERT INTO wp_terms( name, slug, term_group) VALUES ('$Brand', '$slug', 0);";
	echo "<br>$Sql<br>";
	$st=$db->query($Sql);
	
	$TermId = $db->lastInsertId();
	
	$Sql="INSERT INTO wp_term_taxonomy( term_id, taxonomy, description, parent, count) VALUES ( $TermId, 'acf_maps_category', '', 0, 1);";
	$st=$db->query($Sql);	
}
function AddStores(){
	global $db;
	global $StoresList;
	
	$Sql="SELECT distinct imax_store_location.Brand as Brand FROM imax_store_location";
	echo "$Sql<br>";
	$st=$db->query($Sql);
	if ($st){	
		foreach ($st as $Rec){
			$Brand= $Rec['Brand'];
			if (!isset($StoresList[$Brand])){
				echo "$Brand ID=Not defined<BR>";
				CreateNewStore($Brand);
				
			} else {
				echo "$Brand ID=".$StoresList[$Brand]."<BR>";
			}
		}
	}
}
function CreateMapPostMetaData($PostID,$MapDetails,$Contact){
	global $db;
	$Sql="insert into wp_postmeta (post_id,meta_key,meta_value) values ($PostID,'_acf_ngmdl_featured','no');";
	$st=$db->query($Sql);
	$Sql="insert into wp_postmeta (post_id,meta_key,meta_value) values ($PostID,'acf_ngmdl_map_details','$MapDetails');";
	$st=$db->query($Sql);
	$Sql="insert into wp_postmeta (post_id,meta_key,meta_value) values ($PostID,'_acf_ngmdl_map_details','field_541dbac0e5458');";
	$st=$db->query($Sql);
	$Sql="insert into wp_postmeta (post_id,meta_key,meta_value) values ($PostID,'acf_ngmdl_location_contact','$Contact');";
	$st=$db->query($Sql);
	$Sql="insert into wp_postmeta (post_id,meta_key,meta_value) values ($PostID,'_acf_ngmdl_location_contact','field_5421bc7d2042c');";
	$st=$db->query($Sql);
	$Sql="insert into wp_postmeta (post_id,meta_key,meta_value) values ($PostID,'acf_ngmdl_location_url','');";
	$st=$db->query($Sql);
	$Sql="insert into wp_postmeta (post_id,meta_key,meta_value) values ($PostID,'_acf_ngmdl_location_url','field_5421bbfe2042b');";
	$st=$db->query($Sql);
	$Sql="insert into wp_postmeta (post_id,meta_key,meta_value) values ($PostID,'acf_ngmdl_location_other','');";
	$st=$db->query($Sql);
	$Sql="insert into wp_postmeta (post_id,meta_key,meta_value) values ($PostID,'_acf_ngmdl_location_other','field_5421bc7d2142c');";
	$st=$db->query($Sql);
}

function AssignMapToShop($PostID,$StoreName){
	global $db;
	global $StoresList;
	
	print_r($StoresList);
	echo "<BR>$StoreName<BR>";
	$StoreName=str_replace("\'","'",$StoreName);
	if (isset($StoresList[$StoreName])){
		$Id=$StoresList[$StoreName];
		$Sql="INSERT INTO wp_term_relationships(object_id, term_taxonomy_id, term_order) VALUES ($PostID, $Id, 0);";	
		$st=$db->query($Sql);
	}
}
function CreateMapPost($Brand,$StoreName,$Address,$MapDetails,$id_imax_store_location,$Contact){
	global $db;
	global $CurrentURL;
	date_default_timezone_set("UTC");
	$TodayTotonto=date("Y/m/d H:i");
	//date_default_timezone_set("GMT");
	$TodayGMT=$TodayTotonto;
	echo "<BR>$Today<BR>";
	$PostTitle=$StoreName;//"7467 Saint Andrews Rd Ste";
	$PostName=str_replace(" ","-",$PostTitle);
	
	$GUId="$CurrentURL/wordpress/acf_maps/$PostName/";
	$Sql="Insert into wp_posts(post_author,post_date,post_date_gmt,post_title,post_status,comment_status,
			ping_status,post_name,post_modified,post_modified_gmt,post_parent,guid,menu_order,post_type,comment_count)
			Values (1,'$TodayTotonto','$TodayGMT','$PostTitle','publish','closed','closed','$PostName',
			'$TodayTotonto','$TodayGMT',0,'$GUId',0,'acf_maps',0)";
	
	echo "New Map:$Sql";
	$st=$db->query($Sql);
	
	$PostID = $db->lastInsertId();
	echo "Processing Post Id:$PostID";
	CreateMapPostMetaData($PostID,$MapDetails,$Contact);
	AssignMapToShop($PostID,$Brand);
	$Sql="UPDATE imax_store_location SET Processed=1 where id_imax_store_location=$id_imax_store_location;";
	//echo $Sql;
	$st=$db->query($Sql);

}
function CreateMapPosts(){
	global $db;
	$MapLocation=array();
	$Sql="SELECT
			  imax_store_location.id_imax_store_location,
			  imax_store_location.Brand,
			  imax_store_location.StoreName,
			  imax_store_location.Lat,
			  imax_store_location.`Long`,
			  imax_store_location.Address,
			  imax_store_location.Contact,
			  imax_store_location.Processed,
			  imax_store_location.Deleted
			FROM imax_store_location
			WHERE imax_store_location.Lat != ''
			AND   imax_store_location.Processed =0
			AND   imax_store_location.Deleted=0";
	$st=$db->query($Sql);
	foreach ($st as $Rec){
		$id_imax_store_location=$Rec['id_imax_store_location'];	
		$Brand=ucfirst(str_replace("'","\'",$Rec['Brand']));
		$StoreName=str_replace("'","\'",$Rec['StoreName']);		
		$Address=str_replace("'","\'",$Rec['Address']);
		$Lat=$Rec['Lat'];
		$Long=$Rec['Long'];
		$Contact=str_replace("'","\'",$Rec['Contact']);
		$MapLocation['address']=$Address;
		$MapLocation['lat']=$Lat;
		$MapLocation['lng']=$Long;
		$MapDetails=serialize($MapLocation);
		if ($Lat!=""){
			CreateMapPost($Brand,$StoreName,$Address,$MapDetails,$id_imax_store_location,$Contact);		
		}
	}
}
function UploadCSVFile($File){
	$f=fopen($File,"r");	
	while (($Line=fgets($f)) !== false) {
        echo $Line."<BR>";
		ProcessLine($Line);
    }
	fclose($f);	
}
function UploadFromTextArea(){
	$Lines=explode("\n", $_REQUEST['Locations']);
	foreach($Lines as $Line){		
		ProcessLine($Line);		
	}
}
function CleanAddress($Address){
	if (stripos($Address,"Shopping Centre")!==false){
		$Rest=trim(ltrim(strstr($Address,"Shopping Centre"),","));
		$Address=trim(ltrim(strstr($Rest,"Shopping Centre"),"Shopping Centre"));
		
	}
	$Address=str_ireplace('"',"",$Address);
	$Address=str_ireplace("Cnr","",$Address);
	$Address=ltrim($Address,', "-');
	return $Address;
}
function DeleteStore($Store,$Address){
	global $db;
	$Sql="DELETE FROM imax_store_location
	WHERE imax_store_location.StoreName = '$Store'
	AND imax_store_location.Address = '$Address'";
	echo "$Sql<BR>";
	$st=$db->query($Sql);
}

function ProcessLine($Line){
	if (trim($Line)!=""){
		echo "<br>$Line<br>";
		$Line=str_replace("’","\'",$Line);
		$Line=str_replace('\"',"",$Line);
		$Line=str_replace("&nbsp;","",$Line);
		$Line=FixFrench($Line);
		echo "<br>$Line<br>";
		$x=explode(',',$Line);
		$Brand=ucfirst(trim($x[0]));
		$Store=trim($x[1]);
		$Contact=trim($x[2]);
		$Lat=trim($x[3]);
		$Long=trim($x[4]);
		$len=count($x);
		$Address="";
		$Comma="";
		for($i=5;$i<$len;$i++){
			$Address.=$Comma.$x[$i];	
			$Comma=" ";			
		}
		$Address=CleanAddress($Address);
		if ($Store=="") $Store=$Address;
		if (!StoreExists($Store,$Address)){
			AddLocation($Brand,$Store,$Lat,$Long,$Contact,$Address);
		} else {
			DeleteStore($Store,$Address);
			AddLocation($Brand,$Store,$Lat,$Long,$Contact,$Address);
		}
	}
}
function InsertIntoWordpress(){
	global $StoresList;
	AddStores();
	$StoresList=GetStoresList();
	CreateMapPosts();
}
function PopulateTempTable(){
	global $StoresList;
	//global $GoogleLog;
	$ReadFromTextArea=true;
	$StoresList=GetStoresList();
	//$GoogleLog=fopen ("GoogleAPIResponse.txt","w");
	
	if ($ReadFromTextArea){
		echo "<BR>Reading From TextArea<BR>";
		UploadFromTextArea();
	} else {
		echo "<BR>Reading From File<BR>";
		UploadCSVFile("C:\Hany\Sample.csv");
	}
	//fclose($GoogleLog);
	
}
function SetLatLongForUnprocessedRecords(){
	global $db;
	$Sql="SELECT
		  imax_store_location.Lat,
		  imax_store_location.`Long`,
		  imax_store_location.Processed,
		  imax_store_location.Address,
		  imax_store_location.id_imax_store_location,
		  imax_store_location.Deleted,
		  imax_store_location.Contact
		FROM imax_store_location
		WHERE imax_store_location.Lat = ''
		AND imax_store_location.Processed = 0
		AND imax_store_location.Deleted = 0";
	$st=$db->query($Sql);
	foreach($st as $Rec){
		$Id=$Rec['id_imax_store_location'];
		$Address=$Rec['Address'];
		UpdateLocationLatAndLong($Id,$Address);		
	}
}
function DeleteAllMaps(){
	global $db;
	$Sql="UPDATE  wp_posts SET post_status = 'trash' WHERE post_type = 'acf_maps';";
	$st=$db->query($Sql);
	
	//$Sql="UPDATE imax_store_location  SET Processed = 0";
	$Sql="TRUNCATE TABLE imax_store_location;";
	$st=$db->query($Sql);
}
function ListFailedLocations_(){
	global $db;
	$Sql="SELECT
		  imax_store_location.id_imax_store_location,
		  imax_store_location.StoreName,
		  imax_store_location.Lat,
		  imax_store_location.`Long`,
		  imax_store_location.Processed,
		  imax_store_location.Address,
		  imax_store_location.Deleted,
		  imax_store_location.Contact,
		  imax_store_location.GoogleStatus
		FROM imax_store_location
		WHERE imax_store_location.Lat = ''";
	$st=$db->query($Sql);
?>
	<table>
		<tr><th>Store Name</th><th>Contact</th><th>Address</th><th>Google Status</th></tr>
		
<?php	
	foreach($st as $Rec){
?>		
		<tr>
			<td><?php echo $Rec['StoreName']; ?></td>
			<td><?php echo $Rec['Contact']; ?></td>
			<td><?php echo $Rec['Address']; ?></td>			
			<td><?php echo $Rec['GoogleStatus']; ?></td>			
		</tr>
<?php	
	}
?>
	</table>
<?php	
}
function DeleteRecordFromTempTable(){
	$Id=$_REQUEST['Id'];
	echo "DeleteRecordFromTempTable Id=$Id";
}
function GetAddressPostIds($Address){
	global $db;
	$Result=array();
	$Sql="SELECT
		  wp_posts.ID,
		  wp_posts.post_title
		FROM wp_posts
		WHERE wp_posts.post_title = '$Address'
		AND wp_posts.post_status = 'publish'";
	$st=$db->query($Sql);
	foreach($st as $Rec){
		$Result[]=$Rec['ID'];
	}
	return $Result;
}
function GetColocatedStores(){
	global $db;
	$Result=array();
	$Sql="SELECT
		  wp_posts.post_title,
		  count(wp_posts.post_title) AS cnt
		FROM wp_posts
		WHERE wp_posts.post_type = 'acf_maps'
		AND wp_posts.post_status = 'publish'
		GROUP BY wp_posts.post_title
		having cnt>1";
	$st=$db->query($Sql);
	$i=0;
	foreach($st as $Rec){
		$Address=$Rec['post_title'];
		$LatLong=GetLatLong($Address);
		$Result[$i]['Address']=$Address;
		$Result[$i]['Lat']=$LatLong['Lat'];
		$Result[$i]['Long']=$LatLong['Long'];	
		
		$Result[$i]['TopLat']=$LatLong['Lat']+"0.0000200";
		$Result[$i]['BottomLat']=$LatLong['Lat']-"0.0000200";	
		$Result[$i]['RightLong']=$LatLong['Long']+"0.0000250";
		$Result[$i]['LeftLong']=$LatLong['Long']-"0.0000250";
		$Result[$i]['PostsIds']=GetAddressPostIds($Address);
		$i++;
	}
	return $Result;
}
function FixColocationAddress($CurrentAddress){
	global $db;
	$MapLocation=array();
	$Posts=$CurrentAddress['PostsIds'];
	$Count=count($Posts);
	print_r($Posts);

	for ($i=0;$i<$Count;$i++){
		switch($i){
			case 0:
				$Lat=$CurrentAddress['TopLat'];
				$Long=$CurrentAddress['RightLong'];
				break;
			case 1:
				$Lat=$CurrentAddress['TopLat'];
				$Long=$CurrentAddress['LeftLong'];
				break;
			case 2:
				$Lat=$CurrentAddress['BottomLat'];
				$Long=$CurrentAddress['RightLong'];
				break;
			case 3:
				$Lat=$CurrentAddress['BottomLat'];
				$Long=$CurrentAddress['LeftLong'];
				break;
		}

		$MapLocation['address']=$CurrentAddress['Address'];
		$MapLocation['lat']=number_format($Lat,7);
		$MapLocation['lng']=number_format($Long,7);
		$MapDetails=serialize($MapLocation);
		$PostId=$Posts[$i];
		$Sql="UPDATE wp_postmeta Set wp_postmeta.meta_value='$MapDetails' WHERE wp_postmeta.meta_key = 'acf_ngmdl_map_details' and post_id=$PostId ";
		$st=$db->query($Sql);
		echo "<BR>$Sql<BR>";
	}
}
function FixColocations(){
	$AddressesToFix=GetColocatedStores();
	$NumberOfAddresses=count($AddressesToFix);
	for ($i=0;$i<$NumberOfAddresses;$i++){
		$CurrentAddress=$AddressesToFix[$i];
		FixColocationAddress($CurrentAddress);
	}	
}
function CreateLocationTable(){
	global $db;
	$Sql="SHOW TABLES like 'imax_store_location'";
	$st=$db->query($Sql);
	$Exists=false;
	foreach($st as $Rec){
		$Exists=true;
	}	
	if (!$Exists){
		$Sql="CREATE TABLE imax_store_location (
			  id_imax_store_location int(11) NOT NULL AUTO_INCREMENT,
			  StoreName varchar(100) NOT NULL,
			  Lat varchar(30) NOT NULL,
			  `Long` varchar(30) NOT NULL,
			  Address varchar(300) NOT NULL,
			  Processed int(11) NOT NULL DEFAULT 0,
			  Deleted int(11) NOT NULL DEFAULT 0,
			  Contact varchar(25) DEFAULT NULL,
			  GoogleStatus varchar(20) DEFAULT NULL,
			  Brand varchar(40) DEFAULT NULL,
			  PRIMARY KEY (id_imax_store_location)
			);";
		$db->query($Sql);
	}	
}
function ListFailedLocations(){
	global $db;
	$Sql="SELECT
			imax_store_location.Brand,	
			  imax_store_location.StoreName,
			  imax_store_location.Contact,			  
			  imax_store_location.Lat,
			  imax_store_location.Long,
			  imax_store_location.Address
			FROM imax_store_location
			where imax_store_location.Lat=''";
	$st=$db->query($Sql);
	if ($st){
		foreach($st as $Rec){
			$Brand=$Rec['Brand'];
			$StoreName=$Rec['StoreName'];
			$Contact=$Rec['Contact'];
			$Lat=$Rec['Lat'];
			$Long=$Rec['Long'];
			$Address=str_replace(',',' ',$Rec['Address']);
			echo "$Brand,$StoreName,$Contact,$Lat,$Long,$Address\n";
		}
	}
}
function ExportLocations(){

}
function DumpDataFromWordpressTables(){
	global $db;
	$Sql="SELECT
			  wp_terms.name AS Brand,
			  wp_posts.post_title AS StoreName,
			  wp_postmeta.meta_value AS AddressInfo,
			  wp_postmeta_1.meta_value AS Contact
			FROM wp_posts
			  INNER JOIN wp_postmeta
				ON wp_posts.ID = wp_postmeta.post_id
			  LEFT OUTER JOIN wp_term_relationships
				ON wp_posts.ID = wp_term_relationships.object_id
			  LEFT OUTER JOIN wp_term_taxonomy
				ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
			  LEFT OUTER JOIN wp_terms
				ON wp_term_taxonomy.term_id = wp_terms.term_id
			  LEFT OUTER JOIN wp_postmeta wp_postmeta_1
				ON wp_postmeta_1.post_id = wp_posts.ID
			WHERE wp_posts.post_status = 'publish'
			AND wp_posts.post_type = 'acf_maps'
			AND wp_postmeta.meta_key = 'acf_ngmdl_map_details'
			AND wp_postmeta_1.meta_key = 'acf_ngmdl_location_contact'";
	$st=$db->query($Sql);
	if ($st){	
		foreach($st as $Rec){
			$Brand=$Rec['Brand'];
			$StoreName=$Rec['StoreName'];
			$Contact=$Rec['Contact'];
			$AddressComponenets=$Rec['AddressInfo'];
			$AddressInfo=unserialize($AddressComponenets);
			$Lat=$AddressInfo['lat'];
			$Long=$AddressInfo['lng'];
			$Address=$AddressInfo['address'];
			echo "$Brand,$StoreName,$Contact,$Lat,$Long,$Address\n";
		}
	}
}
function DumpDataFromImaxTable(){
	global $db;
	$Sql="SELECT
			imax_store_location.Brand,	
			  imax_store_location.StoreName,
			  imax_store_location.Contact,			  
			  imax_store_location.Lat,
			  imax_store_location.Long,
			  imax_store_location.Address
			FROM imax_store_location
			where imax_store_location.Lat!=''";
	$st=$db->query($Sql);
	if ($st){	
		foreach($st as $Rec){
			$Brand=$Rec['Brand'];
			$StoreName=$Rec['StoreName'];
			$Contact=$Rec['Contact'];
			$Lat=$Rec['Lat'];
			$Long=$Rec['Long'];
			$Address=str_replace(',',' ',$Rec['Address']);
			echo "$Brand,$StoreName,$Contact,$Lat,$Long,$Address\n";
		}
	}
}
CreateLocationTable();
	$Action=$_REQUEST['Action'];
	switch($Action){
		case 'DumpDataFromWordpressTables':
			DumpDataFromWordpressTables();
		break;
		case 'ListFailedLocations':
			ListFailedLocations();
		break;
		case 'DumpDataFromImaxTable':
			DumpDataFromImaxTable();
		break;
		case 'Upload-Test':
			echo $_REQUEST['Locations'];
		break;
		case 'Upload':
			PopulateTempTable();		
			InsertIntoWordpress();
		break;
		case 'ObtainLatLong':
			SetLatLongForUnprocessedRecords();
			InsertIntoWordpress();
		break;
		case 'InsertIntoWordpress':
			InsertIntoWordpress();
		break;
		case 'DeleteAllMaps':
			DeleteAllMaps();
		break;
		case 'ListFailedLocations':
			ListFailedLocations();
		break;
		case 'FixColocation':
			FixColocations();
		break;
		case 'ExportLocations':
			ExportLocations();
		break;
		case 'DeleteRecordFromTempTable':
		break;
	}


