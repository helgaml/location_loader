<?php
$servername=DB_HOST;
$DBName=DB_NAME;
$db = new PDO("mysql:host=$servername;dbname=$DBName", DB_USER, DB_PASSWORD);

function GetUploadedLocations(){
	global $db;
	$Sql="SELECT COUNT(*) as cnt FROM imax_store_location";
	$st=$db->query($Sql);
	foreach($st as $Rec){
		$Result=$Rec['cnt'];
	}
	return $Result;
}
function FailedToObtainLatAndLong(){
	global $db;
	$Sql="SELECT COUNT(*) as cnt FROM imax_store_location WHERE imax_store_location.Lat = ''";
	$st=$db->query($Sql);
	foreach($st as $Rec){
		$Result=$Rec['cnt'];
	}
	return $Result;
}
function NotUploadedInWordpress(){
	global $db;
	$Sql="SELECT COUNT(*) as cnt FROM imax_store_location WHERE imax_store_location.Processed = 0";
	$st=$db->query($Sql);
	foreach($st as $Rec){
		$Result=$Rec['cnt'];
	}
	return $Result;
}
?>
<br>
<div id="Left-div" style="float:left">
<br><span style="font-size:33px;">Location Upload</span><br>
<!--
	<br>
		<strong>Status of Location Uploads</strong><br>
		<span>Uploaded Locations :&nbsp;&nbsp;</span><?php echo GetUploadedLocations(); ?><br>
		<span>Failed to Obtain Lat and Long :&nbsp;&nbsp;</span><?php echo FailedToObtainLatAndLong(); ?> 	
		
		<a href="#" onclick="LocationLoader('ObtainLatLong');">Re-Run</a>&nbsp;&nbsp;		
		<a href="#" onclick="LocationLoader('ListFailedLocations');">List</a><br><br>
		<span>Not Uploaded into Wordpress :&nbsp;&nbsp;</span><?php echo NotUploadedInWordpress(); ?> 		
		<a href="#" onclick="LocationLoader('InsertIntoWordpress');">Insert Into Wordpress</a><br>
	<br>
		<br>	
		<a href="#" onclick="ExportLocations();">Export Locations</a>&nbsp;&nbsp;
	<br>
	-->
	<br>
		<a href="#" onclick="LocationLoader('FixColocation');">Fix Colocation</a>&nbsp;&nbsp;
	<br>

	<br>	
		<a href="#" onclick="DeleteAllLocationsfromWordpress();">Delete ALL locations from Wordpress</a><br>
	<br>
	<br>
	Copy and paste the locations in the below box.
	<BR>
	The Fields are on the following Order:<br>
<strong>Brand , Store Name (if empty, the address will be the default), Contact Number , Lat , Long , Address</strong><br>
The Lat and Long are optional information,if empty, the plugin will obtain it. However, its location must be reserved with the comma.
	<BR><BR>
	
	<!--input type="button" value="List Successfully Loaded Locations " onclick="FillLocationTextArea('DumpDataFromImaxTable');"-->
	<input type="button" value="Select All" onclick="SelectAll();">
	<input type="button" value="List Failed Locations" onclick="FillLocationTextArea('ListFailedLocations');">
	<!--input type="button" value="Dump Data From Posts" onclick="FillLocationTextArea('DumpDataFromPosts');"-->
	
	<input type="button" value="List Ninja Maps Records " onclick="FillLocationTextArea('DumpDataFromWordpressTables');">
	<input type="button" value="Clear" onclick="Clear();">
	
		<input type="hidden" name="Action" value="Upload">
		<textarea id="Locations" name="Locations" rows="4" cols="50" style="margin: 0px; width: 1000px; height: 445px;"></textarea>
	<br>
	<input type="button" value="Upload Locations" onclick="UploadLocations();"><br>
	
	
</div>
<div id="Right-div" style="float:left">
</div>
<div id="Wait-div" style="float:left;display:none;">
<img id="ajax-loader"  src="<?php echo plugin_dir_url( __FILE__ );?>/images/ajax-loader.gif">
</div>