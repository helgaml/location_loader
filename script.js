
function SaveIt(){
	console.log( jQuery("#Locations").val() );
}
function LocationLoader(action){
	WaitImage=jQuery("#Wait-div").html();
	jQuery("#Right-div").html(WaitImage);
	
	jQuery.ajax({
        url: LocationLoaderPluginURL+'/LocationLoaderAjax.php',
        type: 'GET',
        cache: false,
        data: {
           Action: action
        },
        success: function(data){
            jQuery("#Right-div").html(data);
        }
    });	
}
function DeleteAllLocationsfromWordpress(){
	if (confirm("Are you sure that you want to delete All Locations?")){
		WaitImage=jQuery("#Wait-div").html();
		jQuery("#Right-div").html(WaitImage);
		jQuery.ajax({
			url: LocationLoaderPluginURL+'/LocationLoaderAjax.php',
			type: 'GET',
			cache: false,
			data: {
			   Action: 'DeleteAllMaps'
			},
			success: function(data){
				jQuery("#Right-div").html(data);
			}
		});	
	}
}
function UploadLocations(){
	
	WaitImage=jQuery("#Wait-div").html();
	jQuery("#Right-div").html(WaitImage);
	jQuery.ajax({
		url: LocationLoaderPluginURL+'/LocationLoaderAjax.php',
		type: 'POST',
		cache: false,
		data: {
		   Action: 'Upload',
		   Locations: jQuery("#Locations").val()
		},
		success: function(data){
			jQuery("#Right-div").html(data);
		}
	});		
}
function Clear(){
	jQuery("#Locations").val("");
}
function SelectAll(){
	jQuery("#Locations").select();

}
function ExportLocations(){
	WaitImage=jQuery("#Wait-div").html();
	jQuery("#Right-div").html(WaitImage);
	jQuery.ajax({
		url: LocationLoaderPluginURL+'/LocationLoaderAjax.php',
		type: 'POST',
		cache: false,
		data: {
		   Action: 'ExportLocations'
		},
		success: function(data){
			console.log(data);
			jQuery("#Locations").val(data);
			//jQuery("#Right-div").html(data);
		}
	});	
}
function FillLocationTextArea(action){

	jQuery.ajax({
		url: LocationLoaderPluginURL+'/LocationLoaderAjax.php',
		type: 'POST',
		cache: false,
		data: {
		   Action: action
		},
		success: function(data){
			console.log(data);
			jQuery("#Locations").val(data);
			//jQuery("#Right-div").html(data);
		}
	});	
}
function DumpData(){

	jQuery.ajax({
		url: LocationLoaderPluginURL+'/LocationLoaderAjax.php',
		type: 'POST',
		cache: false,
		data: {
		   Action: 'DumpData'
		},
		success: function(data){
			console.log(data);
			jQuery("#Locations").val(data);
			//jQuery("#Right-div").html(data);
		}
	});	
}