<?php
/**
* Plugin Name: Location Loader
* Plugin URI: 
* Description: This plugin uploads locations into Ninja Maps.
* Version: 1.0.0
* Author: Hany Elgaml
* Author URI: 
* License: GPL2
*/
add_action( 'admin_head', 'Add_LocationLoader_Scripts_admin_header' );
function Add_LocationLoader_Scripts_admin_header(){	
	echo "<script>LocationLoaderPluginURL='".plugin_dir_url( __FILE__ )."';</script>";
	echo '<script type="text/javascript" src="'.plugin_dir_url( __FILE__ ).'script.js"></script>';
}

add_action( 'admin_menu', 'register_my_custom_menu_page' );

function register_my_custom_menu_page() {
	add_menu_page( 'custom menu title', 'Location Loader', 'manage_options', 'location_loader/LocationLoaderForm.php', '', plugins_url( 'location_loader/images/add.png' ), 51 );	
}
